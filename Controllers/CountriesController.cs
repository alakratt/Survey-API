using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SurveysApi.Data;
using SurveysApi.Models;

namespace SurveysApi.Controllers
{
    [Route("countries")]
    [ApiController]
    public class CountriesController
    {
        private readonly SurveyAppContext _context;

        public CountriesController(SurveyAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of all countries
        /// </summary>
        /// <returns>List of Countries</returns>
        [HttpGet]
        public async Task<List<Countries>> Read()
        {
            return await _context.Countries.ToListAsync();
        }
    }
}