using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SurveysApi.Data;
using SurveysApi.Models;

namespace SurveysApi.Controllers
{
    [Route("volunteers")]
    [ApiController]
    public class VolunteersController
    {
        private readonly SurveyAppContext _context;

        public VolunteersController(SurveyAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of all volunteers
        /// </summary>
        /// <returns>List of Volunteers</returns>
        [HttpGet]
        public async Task<List<Volunteers>> Read()
        {
            return await _context.Volunteers.ToListAsync();
        }

        /// <summary>
        /// Get a volunteer by the id from the database
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>Volunteers object</returns>        
        [HttpGet("{id}")]
        public async Task<Volunteers> Read(int id)
        {
            return await _context.Volunteers.FirstOrDefaultAsync(v => v.Id == id);
        }

        /// <summary>
        /// Get a volunteer by the pin code
        /// </summary>
        /// <param name="pin_code">string</param>
        /// <returns>Volunteer object</returns>
        [Route("findbycode/{pin_code}")]
        [HttpGet]
        public async Task<Volunteers> Read(string pin_code)
        {
            return await _context.Volunteers.FirstOrDefaultAsync(v => v.PinCode == pin_code);
        }

        /// <summary>
        /// Get a list of Volunteers by their location id
        /// </summary>
        /// <param name="locationId">int</param>
        /// <returns>List of Volunteers</returns>
        [Route("findbylocation/{locationId}")]
        [HttpGet]
        public async Task<List<Volunteers>> FindByLocation(int locationId)
        {
            return await _context.Volunteers.Join(_context.Surveys, v => v.Id, s => s.VolunteerId, (v, s) => new { v, s }).Where(s => s.s.LocationId == locationId)
            .Select(v => new Volunteers
            {
                Id = v.v.Id,
                Name = v.v.Name,
                PinCode = v.v.PinCode,
                Surveys = v.v.Surveys
            }).ToListAsync();
        }

        /// <summary>
        /// Update an existing volunteer from the database.
        /// </summary>
        /// <param name="updatedVolunteer">Volunteers</param>
        /// <returns>Updated Volunteers object, if not then null.</returns>
        [HttpPost]
        public async Task<Volunteers> Update(Volunteers updatedVolunteer)
        {
            _context.Update(updatedVolunteer);
            int isSuccessful = await _context.SaveChangesAsync();
            return isSuccessful == 1 ? updatedVolunteer : null;
        }

        /// <summary>
        /// Create a new volunteer in the database
        /// </summary>
        /// <param name="newVolunteer">Volunteers</param>
        /// <returns>Created Volunteers object, if not then null.</returns>
        [HttpPost("{id}")]
        public async Task<Volunteers> Create(Volunteers newVolunteer, int id)
        {
            int pinCode = await _context.Volunteers.MaxAsync(v => Convert.ToInt32(v.PinCode));
            string strPinCode = null;

            strPinCode = (pinCode + 1).ToString();

            switch (strPinCode.Length)
            {
                case 1:
                    newVolunteer.PinCode = $"00{strPinCode}";
                    break;
                case 2:
                    newVolunteer.PinCode = $"0{strPinCode}";
                    break;
            }

            await _context.AddAsync(newVolunteer);
            int isSuccessful = await _context.SaveChangesAsync();
            if (isSuccessful == 1)
            {
                Surveys newSurvey = new Surveys()
                {
                    LocationId = id,
                    VolunteerId = newVolunteer.Id,
                    IsComplete = false
                };
                _context.Surveys.Add(newSurvey);
                isSuccessful = await _context.SaveChangesAsync();
                newVolunteer.Surveys.Add(newSurvey);
            }
            return isSuccessful == 1 ? newVolunteer : null;
        }

        /// <summary>
        /// Gets a list of Volunteers objects filtered by a search string.
        /// Search value could be either their name or pin code.
        /// </summary>
        /// <param name="searchString">string</param>
        /// <returns>List of Locations objects</returns>
        [Route("search/{searchString}")]
        [HttpGet]
        public async Task<List<Volunteers>> Search(string searchString)
        {
            return await _context.Volunteers.Where(vol => vol.Name.ToLower().Contains(searchString.ToLower()) || vol.PinCode.ToLower() == searchString.ToLower()).ToListAsync();
        }
    }
}